﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MatBlazor;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.SignalR.Client;


namespace BlazorRTColorChange.Client.Pages
{
    public partial class Index : IAsyncDisposable
    {

        [Inject] 
        NavigationManager NavigationManager { get; set; }

        private bool isControlled;

        private HubConnection hubConnection;
        private List<string> colorValues = new List<string>();
        private string userInput = "";
        private string colorInput;

        private string Color { get => colorInput; set => SetColor(value); }

        MatTheme theme1 = new MatTheme()
        {
            Primary = MatThemeColors.Orange._500.Value,
            Secondary = MatThemeColors.BlueGrey._500.Value
        };

        protected override async Task OnInitializedAsync()
        {
            hubConnection = new HubConnectionBuilder()
                .WithUrl(NavigationManager.ToAbsoluteUri("/chathub"))
                .Build();

            hubConnection.On<string, string>("ReceiveColorChage", (user, message) =>
            {
                var encodedMsg = $"{user}: {message}";
                colorValues.Add(encodedMsg);
                StateHasChanged();
            });

            await hubConnection.StartAsync();
        }

        Task Send() =>
            hubConnection.SendAsync("ChangeColor", userInput, colorInput);

        public bool IsConnected =>
            hubConnection.State == HubConnectionState.Connected;

        public async ValueTask DisposeAsync()
        {
            await hubConnection.DisposeAsync();
        }

        public void SetControlled()
        {
            isControlled = true;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void SetColor(string color)
        {
            colorInput = color;

            if (IsConnected)
            {
                Console.WriteLine("Sent Color change to " + colorInput);
                theme1.Primary = colorInput;
                Send();
            }
            
        }
    }
}
