﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace BlazorRTColorChange.Server.Hubs
{
    public class ColorControlHub : Hub
    {
        public async Task ChangeColor(string user, string color)
        {
            await Clients.All.SendAsync("ReceiveColorChage", user, color);
        }
    }
}
